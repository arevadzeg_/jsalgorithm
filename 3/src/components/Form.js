import "../css/From.css";
import { useState } from "react";

const Form = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [text, setText] = useState("");
  const [checkbox, setCheckbox] = useState(false);
  const [data, setData] = useState({
    email: "Your email",
    password: "Your Password",
    text: "Your Imagination",
    checkbox: false,
  });

  const emailData = (e) => {
    setEmail(e.target.value);
  };
  const passwordData = (e) => {
    setPassword(e.target.value);
  };
  const textData = (e) => {
    setText(e.target.value);
  };
  const checkBoxData = (e) => {
    setCheckbox(e.target.checked);
  };

  const getWholeData = () => {
    setData({
      email: email,
      password: password,
      text: text,
      checkbox: checkbox,
    });
  };

  return (
    <>
      <form>
        <input
          type="text"
          placeholder="Enter your email"
          class="emailInput"
          onChange={emailData}
        />
        <input
          type="password"
          placeholder="Enter your password"
          class="passwordInput"
          onChange={passwordData}
        />
        <textarea class="textInput" onChange={textData}></textarea>
        <h2>Do you agree to out terms and condtions</h2>
        <input type="checkbox" class="checkBox" onChange={checkBoxData} />
        <button class="btn" onClick={getWholeData} type="button">
          Register
        </button>
      </form>
      <div>
        <h2>Email: {data.email}</h2>
        <h2>Password: {data.password}</h2>
        <h2>Text: {data.text}</h2>
        <h2>CheckBox: {String(data.checkbox)}</h2>
      </div>
    </>
  );
};

export default Form;
